# Django

## Crud with django


#How to run:

1. In the terminal:

git clone https://gitlab.com/tockh/django.git

2. cd into the django directory

git checkout -b dev


3. create an environment:

>python -m venv env

4. activate the environment: 

>env\Scripts\activate

5. install dependencies: 

>pip install Django

6. Create the project: 

6.1, First: crud is the name defined for the project
6.2,return to the root directory 

>django-admin startproject crud .

7. Create an app: 
 
7.1, First, app is the name defined for the app 

>python manage.py startapp app 

8. Start the app: 

>python manage.py runserver



